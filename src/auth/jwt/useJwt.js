// ** Core JWT Import
import jwtFunction from '@src/@core/auth/jwt/useJwt'

const { jwt } = jwtFunction({})

export default jwt
