// ** JWT Service Import
import JwtService from './jwtService'

// ** Export Service as jwtFunction
export default function jwtFunction(jwtOverrideConfig) {
  const jwt = new JwtService(jwtOverrideConfig)

  return {
    jwt
  }
}
